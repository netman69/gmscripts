// ==UserScript==
// @name     Away with Youtube Shorts
// @version  1
// @grant    none
// @match    https://www.youtube.com/*
// @run-at document-start
// ==/UserScript==

function noshort() {
  var url = window.location.toString();
  if (url.match(/^https:\/\/[^\/]*\/shorts\//)) {
    history.replaceState({}, "", url.replace(/^https:\/\/[^\/]*\/shorts\//, "/watch?v="));
    location.reload();
  }
}

noshort();

let lastUrl = location.href; 
new MutationObserver(() => {
  const url = location.href;
  if (url !== lastUrl) {
    lastUrl = url;
    noshort();
  }
}).observe(document, {subtree: true, childList: true});
