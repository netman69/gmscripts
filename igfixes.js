// ==UserScript==
// @name     Instagram Fixes
// @version  1
// @grant    none
// @match    https://www.instagram.com/*
// ==/UserScript==

// Allow videos to play in invisible tabs.
window.addEventListener('visibilitychange', function (event) {
    event.stopPropagation();
}, true);

// Videos are inserted by javascript somehow.
document.body.addEventListener('DOMNodeInserted', function () {
  // Get video element or bail out if there is none.
  var vids = document.querySelectorAll('video');
  if (!vids)
    return;
    vids.forEach(function (vid) {
    // Show video controls.
    vid.setAttribute('controls', '');
    vid.style.zIndex = 999;
    vid.style.display = 'initial';

    // Stop videos from looping.
    vid.addEventListener('play', function () { vid.removeAttribute('loop'); });

    // Do not start the video muted, this also stops autoplay (at least for me).
    vid.muted = false;
  });
});
